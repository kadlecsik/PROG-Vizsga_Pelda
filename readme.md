Egy iskolai osztálykirándulás árát a 2013/14-es tanévben a tanulók részletekben fizetik be. Minden tanuló minden befizetése egy szöveges állomány egy-egy sorában található, amelyik tartalmazza a tanuló nevét (sztring), a befizetés dátumát (EEEE.HH.NN formájú sztring) és összegét (természetes szám). Egy soron belül az adatokat elválasztójelek (szóközök, tabulátor jelek) határolják egymástól. A sorok a tanulók nevei szerint, azon belül a befizetési dátumok időrendje szerint rendezettek.
Példa néhány egymást követő sorra:

Feri   2013.11.13   2500   
Feri   2014.01.23   3000   
Feri   2014.03.11   1500
Juli    2013.11.21   4000
Juli    2014.02.15   1500

Megfelelt szintű (közepes) a munkája akkor, ha
1. kilistázza a képernyőre, hogy az egyes tanulók mennyit fizettek be eddig,
2. keres olyan tanulót, aki egyenlő részletekben fizette ki a kirándulást.
Ezeken kívül a programja kielégíti az alábbi követelményeket:
- helyesen működik a programja (felteheti, hogy a tanuló neve egyetlen, elválasztójeleket nem tartalmazó sztring), és az üres fájl és a nem létező fájl esetét is kezeli;
- a program ciklusai tanult programozási tételekből származnak;
- egy osztályt készít a szöveges állomány olvasásához;
- a szöveges állományt csak egyszer nyitja meg olvasásra és nem használ a szöveges állomány sorainak számától függő méretű változót.
