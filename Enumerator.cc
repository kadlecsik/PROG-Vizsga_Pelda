#include "Enumerator.hh"


/*
    Konstruktor
*/
Enumerator::Enumerator(const std::string& filePath)
{
    inputStream.open(filePath.c_str()); //Megnyitja a fájlt
    if(inputStream.fail())
    {
        std::cerr << "A fajl nem nyithato meg."; // Ha nem sikerült hibaüzenet
        exit(1);                          // és kilépés
    }
    end = false;    //Egyébként feltehető, hogy nem értünk a fájl végére, mert még el sem kezdtünk belőle olvasni
}

/*
    Destruktor
*/
Enumerator::~Enumerator()
{
    inputStream.close(); //Bezárja a streamet
}

/*
    Felsoroló inicializálása
*/
void Enumerator::init()
{
    inputStream >> cache;   //Első sor beolvasása
    next();                 //Feldolgozás megkezdése
}


void Enumerator::next()
{
    if(inputStream.fail()) //Ha az előző beolvasás sikertelen volt
    {
        end = true; //Végére értünk a fájlnak
        return;     //A next függvény már ne fusson le
    }

    //Ha az előző beolvasás sieres volt

    currentData.name = cache.name; //Az előállított adatban a név legyen ugyanaz, mint a beolvasott sorban, ez egy azonosító, minden azonosító-szerű adatnál ez kell
    currentData.sumOfPayments = 0; //Minden más adatot nullázni
    currentData.inEqualFractions = true; //Vagy a progtétel szerinti kezdőértékre beállítani (itt pl opt. linker)

    firstPayment = cache.amount; //Végül az egyéb adatokat is be kell állítani

    while(!inputStream.fail() && cache.name == currentData.name) //Leáll az olvasás, ha a felsoroló elfogyott, vagy más azonosítójú adatot olvastunk be
    {
        currentData.sumOfPayments += cache.amount; //1. progtétel: összegzés
        if(cache.amount != firstPayment) //2. progtétel opt. linker
        {
            currentData.inEqualFractions = false;
        }
        inputStream >> cache; //A következő sor beolvasása
    }
}

std::istream& operator>>(std::istream& in, Record& record)
{
    in >> record.name >> record.date >> record.amount;

    return in;
}
