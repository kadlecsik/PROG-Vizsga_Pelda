#include <iostream>

#include "Enumerator.hh"

int main()
{
    Enumerator e("input.txt"); //Elindítja a felsorolást egy fájlon

    e.init();

    if(e.isEnd()) //Ha inicializálás után rögtön end volt, üres a fájl
    {
        std::cout << "Ures fajl.\n";
    }
    else
    {
        //Egyébként a feladat összes befizetés tanulónként + passz. linker

        std::string name; //A tanuló neve;
        bool found = false; //Volt-e találat

        for(;!e.isEnd();e.next())
        {
            std::cout << e.current().name << " : " << e.current().sumOfPayments << "Ft.\n";
            if(!found && e.current().inEqualFractions) //Ha a !found kimarad az utlsót adja meg, így az elsőt
            {
                found = true;
                name = e.current().name;
            }
        }

        if(found)
        {
            std::cout << "Egyenlo reszletekben fizetett: " << name <<  "\n";
        }
        else
        {
            std::cout << "Nem volt, aki egyenlo reszletekben fizetett volna.";
        }
    }





    return 0;
}
