#ifndef ENUMERATOR_HH_INCLUDED
#define ENUMERATOR_HH_INCLUDED

#include <iostream>
#include <fstream>

/*
    A felsoroló által előállított adat, minden részadathoz tartoznia kell egy változónak.
*/
struct Result
{
    std::string name; // Tanuló neve
    int sumOfPayments; // Összes befizetés
    bool inEqualFractions; // Egyenlő részletekben fizetett-e
};

/*
    A bemeneti fájl egy sorát reprezentálja, minden oszlophoz tartozzon egy változó
*/
struct Record
{
    std::string name; //Tanuló neve
    std::string date; //Befizetés dátuma
    int amount; //Befizetés összege
};

/*
    Egy sort beolvasó operátor
*/
std::istream& operator>>(std::istream& in, Record& record);

class Enumerator
{
private:

    /*
    Minden feladatnál szükséges adattagok:
    */

    std::ifstream inputStream;

    Record cache;   //Beolvasott, feldolozandó adat
    Result currentData; //A felsoroló által előállított adat

    bool end;   //Elfogyott-e a felsoroló

    /*
    Egyedi, minden feadatnál különböző adattagok:
    */

    int firstPayment; //A tanuló első befizetésének összege
public:
    Enumerator(const std::string& filePath);
    ~Enumerator();

    void init();
    void next();
    Result current() const
    {
        return currentData;
    }
    bool isEnd() const
    {
        return end;
    }
};


#endif // ENUMERATOR_HH_INCLUDED
